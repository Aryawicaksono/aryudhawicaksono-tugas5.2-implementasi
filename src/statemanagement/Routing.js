import React from 'react';
import Summary from './Summary';
import AddData from './AddData';
import Keranjang from './Keranjang';

import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Summary" component={Summary} />
        <Stack.Screen name="AddData" component={AddData} />
        <Stack.Screen name="Keranjang" component={Keranjang} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
