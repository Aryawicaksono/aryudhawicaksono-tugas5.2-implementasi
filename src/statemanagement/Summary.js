import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';

export default function Summary({navigation, route}) {
  // const [showButton, setShowButton] = useState(false);
  const {product} = useSelector(state => state.data);
  console.log(product.length);
  // if (product.length > 0) {
  //   useEffect(() => {
  //     setShowButton(true);
  //   });
  // }
  const [isScrollEnabled, setIsScrollEnabled] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setIsScrollEnabled(true);
    }, 3000);
  });
  return (
    <View style={styles.Container}>
      <View style={styles.Header}>
        <TouchableOpacity>
          <Image
            style={styles.Eikon}
            source={require('../assets/icon/Back.png')}
          />
        </TouchableOpacity>
        <Text style={styles.TextDes}>Summary</Text>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        scrollEnabled={isScrollEnabled}
        data={product}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('AddData', {item})}>
            <View style={styles.Body}>
              <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
                Data Customer
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    marginTop: 10,
                    fontSize: 14,
                    fontWeight: '400',
                    color: '#201F26',
                  }}>
                  {item.fullname}
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    marginLeft: 9,
                    fontSize: 14,
                    fontWeight: '400',
                    color: '#201F26',
                  }}>
                  ({item.phone})
                </Text>
              </View>
              <Text
                style={{
                  marginTop: 6,
                  fontSize: 14,
                  fontWeight: '400',
                  color: '#201F26',
                }}>
                {item.address}
              </Text>
              <Text style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
                {item.email}
              </Text>
            </View>
            <View style={styles.Body}>
              <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
                Alamat Outlet Tujuan
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    marginTop: 10,
                    fontSize: 14,
                    fontWeight: '400',
                    color: '#201F26',
                  }}>
                  {item.shopName}
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    marginLeft: 9,
                    fontSize: 14,
                    fontWeight: '400',
                    color: '#201F26',
                  }}>
                  ({item.shopPhone})
                </Text>
              </View>
              <Text
                style={{
                  marginTop: 6,
                  fontSize: 14,
                  fontWeight: '400',
                  color: '#201F26',
                }}>
                {item.shopAddress}
              </Text>
            </View>
            <View style={styles.Body}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.Shoe}
                  source={require('../assets/icon/Shoe.png')}
                />
                <View style={styles.ShoeText}>
                  <Text style={styles.TextH}>New Balance - Pink Abu - 40</Text>
                  <Text style={styles.TextBod}>Cuci Sepatu</Text>
                  <Text style={styles.TextBod}>Note:-</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      {product.length > 0 ? (
        <TouchableOpacity
          style={{
            marginHorizontal: 20,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Keranjang')}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      ) : null}

      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnFloating}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 20,
    paddingRight: '25%',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  TextH: {
    fontSize: 12,
    fontWeight: '500',
    color: '#000000',
  },
  TextBod: {
    marginTop: 11,
    fontWeight: '400',
    color: '#737373',
    fontSize: 12,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
  Shoe: {
    height: 84,
    width: 84,
    resizeMode: 'contain',
  },
  ShoeText: {
    marginLeft: 17,
  },
  btnFloating: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
