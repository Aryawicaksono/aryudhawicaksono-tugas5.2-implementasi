import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

const Keranjang = ({navigation, route}) => {
  const {product} = useSelector(state => state.data);
  console.log(product);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate('Summary')}>
          <Image
            style={styles.icon}
            source={require('../assets/icon/Back.png')}
          />
        </TouchableOpacity>
        <View style={{width: 10}} />
        <Text style={styles.headertext}>Transaksi</Text>
      </View>
      <View style={{height: 5}} />
      <FlatList
        data={product}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity onPress={() => navigation.navigate('Summary')}>
            <View style={styles.body}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.dateText}>{item.date}</Text>
                <View style={{width: 11}} />
                <Text style={styles.dateText}>{item.time}</Text>
              </View>
              <View style={{height: 10}} />
              <Text style={styles.markText}>New Balance-Pink-Abu-40</Text>
              <Text style={styles.fillText}>Cuci Sepatu</Text>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.fillText}>Kode Reservasi</Text>
                  <View style={{width: 10}} />
                  <Text style={styles.codeText}>{item.id}</Text>
                </View>
                <View style={styles.status}>
                  <Text style={styles.statusText}>Reserved</Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default Keranjang;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#F6F8FF'},
  header: {
    height: 56,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 21,
    paddingVertical: 16,
    flexDirection: 'row',
  },
  body: {
    marginHorizontal: 15,
    paddingHorizontal: 10,
    paddingTop: 18,
    paddingBottom: 21,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
  },
  status: {
    height: 21,
    width: 81,
    backgroundColor: '#F29C1F29',
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {height: 24, width: 24, tintColor: '#000000', resizeMode: 'contain'},
  dateTime: {flexDirection: 'row'},
  headertext: {fontSize: 18, fontWeight: '700', color: '#201F26'},
  dateText: {fontSize: 12, fontWeight: '500', color: '#BDBDBD'},
  markText: {fontSize: 12, fontWeight: '500', color: '#201F26'},
  fillText: {fontSize: 12, fontWeight: '400', color: '#201F26'},
  codeText: {fontSize: 12, fontWeight: '700', color: '#201F26'},
  statusText: {fontSize: 12, fontWeight: '400', color: '#FFC107'},
});
